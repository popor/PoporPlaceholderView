#
# Be sure to run `pod lib lint PoporPlaceholderView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name        = 'PoporPlaceholderView'
  s.version     = '1.12'
  s.summary     = 'PlaceholderView uesd for UITableView and UICollectionView.'

  s.homepage    = 'https://gitee.com/popor/PoporPlaceholderView'
  s.license     = { :type => 'MIT', :file => 'LICENSE' }
  s.author      = { 'popor' => '908891024@qq.com' }
  s.source      = { :git => 'https://gitee.com/popor/PoporPlaceholderView.git', :tag => s.version.to_s }
  
  s.ios.deployment_target = '9.0'
  
  s.source_files = 'PoporPlaceholderView/Classes/*.{h,m}'
  
  # 不再使用sdk图片, 有点多余
  #s.resource     = 'PoporPlaceholderView/Classes/PoporPlaceholderView.bundle'
  
  s.dependency 'Masonry'
  s.dependency 'MJRefresh'
  
end
