//
//  NSObject+pSwizzling_ppv.m
//  PoporFoundation
//
//  Created by popor on 2017/10/25.
//  Copyright © 2017年 popor. All rights reserved.
//

#import "NSObject+pSwizzling_ppv.h"
#import <objc/message.h>

@implementation NSObject (pSwizzling_ppv)

// 交换 NSObject 方法
+ (void)ppv_ChangeSelector:(SEL _Nonnull)originalSelector
                toSelector:(SEL _Nonnull)swizzledSelector
{
    
    if (!originalSelector || !swizzledSelector) {
        NSLog(@"交换方法失败! %s", __func__);
        return;
    }
    
    Class class           = [self class];
    Method originalMethod = class_getInstanceMethod(class, originalSelector); //原有方法
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector); //替换原有方法的新方法
    
    if (!originalMethod || !swizzledMethod) {
        NSLog(@"交换方法失败! %s", __func__);
        return;
    }
    
    //先尝试給源SEL添加IMP，这里是为了避免源SEL没有实现IMP的情况
    BOOL didAddMethod = class_addMethod(class, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod));
    if (didAddMethod) {//添加成功：说明源SEL没有实现IMP，将源SEL的IMP替换到交换SEL的IMP
        class_replaceMethod(class,swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
    } else {//添加失败：说明源SEL已经有IMP，直接将两个SEL的IMP交换即可
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}


@end
