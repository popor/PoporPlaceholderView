//
//  NSObject+pSwizzling_ppv.h
//  PoporFoundation
//
//  Created by popor on 2017/10/25.
//  Copyright © 2017年 popor. All rights reserved.
//
/**
 OCDynamicHookUtils
 https://github.com/enefry/OCDynamicHookUtils
 */

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (pSwizzling_ppv)

// 交换 NSObject 方法
+ (void)ppv_ChangeSelector:(SEL _Nonnull)originalSelector
                toSelector:(SEL _Nonnull)swizzledSelector;

@end

NS_ASSUME_NONNULL_END
