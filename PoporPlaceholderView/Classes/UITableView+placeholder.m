//
//  UITableView+placeholder.m
//  PoporPlaceholderView
//
//  Created by popor on 2018/4/18.
//  Copyright © 2018年 popor. All rights reserved.
//

#import "UITableView+placeholder.h"

#import <objc/runtime.h>
#import "NSObject+pSwizzling_ppv.h"
#import <MJRefresh/MJRefresh.h>


@implementation UITableView (placeholder)
@dynamic placeHolderView;
@dynamic placeHolderEmptyBlock;
@dynamic placeHolderReloadBlock;
@dynamic showPlaceholder;

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [objc_getClass("UITableView") ppv_ChangeSelector:@selector(reloadData) toSelector:@selector(gy_reloadData)];
    });
}

- (void)gy_reloadData {
    [self gy_checkEmpty];
    
    CGFloat y = self.contentOffset.y;
    [self gy_reloadData];
    
    if (self.showPlaceholder) {
        if (y == 0) {
            [self addSubview:self.placeHolderView];
            [self.placeHolderView freshStatus];
        } else {
            CGFloat delay = y/100.0;
            delay = MIN(0.3, y);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self addSubview:self.placeHolderView];
                [self.placeHolderView freshStatus];
            });
        }
    }else{
        [self.placeHolderView removeFromSuperview];
    }
    
    if (self.placeHolderReloadBlock) {
        self.placeHolderReloadBlock(self);
    }
}

- (void)gy_checkEmpty {
    if (!self.placeHolderView) {
        return;
    }
    BOOL isEmpty = YES;
    if (self.placeHolderEmptyBlock) {
        isEmpty = self.placeHolderEmptyBlock(self, self.placeHolderView);
    } else {
        id<UITableViewDataSource> src = self.dataSource;
        NSInteger sections = 1;
        if ([src respondsToSelector:@selector(numberOfSectionsInTableView:)]) {
            sections = [src numberOfSectionsInTableView:self];
        }
        for (int i = 0; i < sections; i++) {
            NSInteger rows = [src tableView:self numberOfRowsInSection:i];
            if (rows>0) {
                isEmpty = NO;
                break;
            }
        }
    }
    
    self.showPlaceholder = isEmpty;
    
    self.mj_footer.hidden = isEmpty;
}

#pragma mark - set get
- (void)setPlaceHolderView:(UIView *)placeHolderView {
    objc_setAssociatedObject(self, @"placeHolderView", placeHolderView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    [self reloadData];
}

- (UIView *)placeHolderView {
    return objc_getAssociatedObject(self, @"placeHolderView");
}

- (void)setPlaceHolderEmptyBlock:(UITableViewPlaceHolderEmptyBlock)placeHolderEmptyBlock {
    objc_setAssociatedObject(self, @"placeHolderEmptyBlock", placeHolderEmptyBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (UITableViewPlaceHolderEmptyBlock)placeHolderEmptyBlock {
    return objc_getAssociatedObject(self, @"placeHolderEmptyBlock");
}

- (void)setPlaceHolderReloadBlock:(UITableViewPlaceHolderReloadBlock)placeHolderReloadBlock {
    objc_setAssociatedObject(self, @"placeHolderReloadBlock", placeHolderReloadBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (UITableViewPlaceHolderReloadBlock)placeHolderReloadBlock {
    return objc_getAssociatedObject(self, @"placeHolderReloadBlock");
}

- (void)setShowPlaceholder:(BOOL)showPlaceholder {
    objc_setAssociatedObject(self, @"showPlaceholder", @(showPlaceholder), OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (BOOL)isShowPlaceholder {
    NSNumber * number = objc_getAssociatedObject(self, @"showPlaceholder");
    if (number) {
        return number.boolValue;
    } else {
        return NO;
    }
}

@end
